# Dlog
Selfbot de logs para o Discord

**Devido a alguns problemas, mudamos para o GitHub. Clique [aqui](https://github.com/Craftzao/dlog) para ir para lá**
------
**AVISO:** Usar um selfbot pode resultar em ban do discord, por isso não nos responsabilizamos se a sua conta for banida.
## Como instalar:
Para ver como instalar o dlog, clique [aqui](https://bitbucket.org/Craftzao/dlog/wiki/Instalação)
Depois de instalar, já pode inicia-lo com o ``iniciar.bat`` ou usando ``node app.js`` no terminal na pasta do dlog

To see this README in english, click [here](https://bitbucket.org/Craftzao/dlog/src/364e7dbc2575ce8c1bbb177ee72dd7b5286f17d4/README-EN.md?at=master&fileviewer=file-view-default)