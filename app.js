const Discord = require('discord.js');
const client = new Discord.Client();
const config = require('./config.json');
var data = new Date();
var version = "v1.2.0"
var dataconsole = "[" + data.getDate() + "/" + data.getMonth() + "/" + data.getFullYear() + "] - " + data.getHours() + ":" + data.getMinutes() + ":" + data.getSeconds() + "] "
if(config.login === "") {
   if(config.linguagem === 'pt') return console.log(dataconsole + "[DLog] Você primeiro precisa colocar o seu token de autenticação do Discord na config.json");
   if(config.linguagem === 'en') return console.log(dataconsole + "[Dlog] you first need to put your Discord authentication token in config.json");
   
}
client.on('ready', () => {
    if(config.linguagem === "pt") {
        console.log("");
        console.log(dataconsole + "DLog versão " + version + " iniciando...");
        console.log("");
        console.log(dataconsole + "Linguagem: " + config.linguagem);
        console.log("")
        console.log(dataconsole + "DLog iniciado");
        console.log("");
        return;
    } 
    if(config.linguagem === "en") {
     console.log("");
     console.log(dataconsole + "DLog version " + version + " starting...");
     console.log("");
     console.log(dataconsole + "Language: " + config.linguagem);
     console.log("")
     console.log(dataconsole + "DLog started");
     console.log("");
     return; 
    }
});

client.on('message', (msg) => {    
    let argumento =  msg.content.split(" ").slice(0).join(" ");
    if(argumento === "") return;
    if(!msg.guild) {
        if(config.linguagem === "en") return console.log(dataconsole + "[Private] " + msg.author.username + ": " + argumento);
        if(config.linguagem === "pt") return console.log(dataconsole + "[Privado] " + msg.author.username + ": " + argumento);
        return;
    }

    if(msg.guild.id === config.serverignoreid && (config.serverignore === "ativado")) return;
    if(config.firstserver === 'ativado' && (msg.guild.id === config.firstserverid)) return console.log(dataconsole + "[" + msg.guild.name + "] " + "[" + msg.channel.name + "] " + msg.author.username + ": " + argumento);
    var log = dataconsole + "[" + msg.guild.name + "] " + "[" + msg.channel.name + "] " + msg.author.username + ": " + argumento
    console.log(log);
});

client.login(config.login);